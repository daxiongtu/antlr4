1.下载
$ cd /usr/local/lib/
$ curl -O http://www.antlr.org/download/antlr-4.8-complete.jar

jar下载地址
https://www.antlr.org/download.html

可以看到该目录下出现一个jar包
-rw-r--r--   1 lcc  staff     0  6 29 16:18 antlr-4.0-complete.jar
2.修改环境变量
$ vim .bash_profile
export CLASSPATH=".:/usr/local/lib/antlr-4.8-complete.jar:$CLASSPATH"
alias antlr4='java -jar /usr/local/lib/antlr-4.8-complete.jar'
alias grun='java org.antlr.v4.gui.TestRig'
lcc@lcc ~$ source .bash_profile

3.测试
$ antlr4
ANTLR Parser Generator  Version 4.0
 -o ___              specify output directory where all output is generated
 -lib ___            specify location of grammars, tokens files
 -atn                generate rule augmented transition network diagrams
 -encoding ___       specify grammar file encoding; e.g., euc-jp
 -message-format ___ specify output style for messages in antlr, gnu, vs2005
 -long-messages      show exception details when available for errors and warnings
 -listener           generate parse tree listener (default)
 -no-listener        don't generate parse tree listener

~$ grun
java org.antlr.v4.runtime.misc.TestRig GrammarName startRuleName
  [-tokens] [-tree] [-gui] [-ps file.ps] [-encoding encodingname]
  [-trace] [-diagnostics] [-SLL]
  [input-filename(s)]
Use startRuleName='tokens' if GrammarName is a lexer grammar.
Omitting input-filename makes rig read from stdin.
Exception in thread "main" java.lang.NullPointerException
    at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:311)
    at java.lang.ClassLoader.loadClass(ClassLoader.java:357)
    at org.antlr.v4.runtime.misc.TestRig.process(TestRig.java:157)
    at org.antlr.v4.runtime.misc.TestRig.main(TestRig.java:142)


该错误不影响，这是正确的，因为没加参数
4.开始测试
$ pwd
/Users/lcc/IdeaProjects/JdkSource/src/main/java/com/antlr/hellow
$

该目录下编写文件
$ vim Hello.g4
grammar Hello;
r  : 'hello' ID ;
ID : [a-z]+ ;
WS : [ \t\r\n]+ -> skip ;

执行antlr4生成文件，并且编译java源代码，注意在IDEA工具中，编译的class文件不在同一个目录
$ antlr4 Hello.g4
$ ls
Hello.g4               Hello.tokens           HelloBaseListener.java HelloLexer.java        HelloLexer.tokens      HelloListener.java     HelloParser.java
$ javac *.java
$ ll
total 96
drwxr-xr-x  14 lcc  staff   448 Jun 29 16:13 ./
drwxr-xr-x   6 lcc  staff   192 Jun 29 14:06 ../
-rw-r--r--   1 lcc  staff    72 Jun 29 16:07 Hello.g4
-rw-r--r--   1 lcc  staff    27 Jun 29 16:12 Hello.tokens
-rw-r--r--   1 lcc  staff   794 Jun 29 16:13 HelloBaseListener.class
-rw-r--r--   1 lcc  staff   641 Jun 29 16:12 HelloBaseListener.java
-rw-r--r--   1 lcc  staff  2518 Jun 29 16:13 HelloLexer.class
-rw-r--r--   1 lcc  staff  2401 Jun 29 16:12 HelloLexer.java
-rw-r--r--   1 lcc  staff    27 Jun 29 16:12 HelloLexer.tokens
-rw-r--r--   1 lcc  staff   304 Jun 29 16:13 HelloListener.class
-rw-r--r--   1 lcc  staff   251 Jun 29 16:12 HelloListener.java
-rw-r--r--   1 lcc  staff   869 Jun 29 16:13 HelloParser$RContext.class
-rw-r--r--   1 lcc  staff  3022 Jun 29 16:13 HelloParser.class
-rw-r--r--   1 lcc  staff  2533 Jun 29 16:12 HelloParser.java
$ 


然后我们执行命令，输入hello parrt，然后换行，按下ctrl+d（UNIX系统上是ctrl+d，在windows下是ctrl+z）,可以看到输出结果
$ grun Hello r -tokens
hello parrt
[@0,0:4='hello',<1>,1:0]
[@1,6:10='parrt',<2>,1:6]
[@2,12:11='<EOF>',<-1>,2:0]

最后看图，输入入下命令
$ grun Hello r -gui
hello parrt
^D

同样需要输入 hello parrt，换行，按下ctrl+d，桌面如图

